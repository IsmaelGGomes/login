<?php

session_start();
include('conexao.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <title>Painel</title>
</head>

<body>
    <h3><b>Bem vindo ao Painel, <?php echo $_SESSION['nome']; ?></b></h3>

    <p>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-xs-6">

                                <p>
                                <form action="" method="GET">

                                    <input type="text" name="name" size="8" placeholder=" Insira o ID">
                                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                </form>
                                </p>

                            </div>
                            <div class="col col-xs-6 text-right">

                                <a href="registro.php?=cadastro" > <button type="button" class="btn btn-sm btn-primary btn-create">Novo Cadastro</button> </a>
                                <? // header("Location: login.php")
                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-list">
                            <thead>
                                <tr>
                                    <th><em class="fa fa-cog"></em></th>
                                    <th class="hidden-xs">ID</th>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Senha</th>

                                </tr>
                            </thead>
                            <tbody>

                                <tr>

                                    <?php

                                    $id_pesquisa = $pesquisar = filter_input(INPUT_GET, 'id');
                                    
                                    $id_pesquisa_name = $pesquisar = filter_input(INPUT_GET, 'name');
                                    $resultado_pesquisa = "SELECT * FROM acesso_novo WHERE id= $id_pesquisa_name";
                                    $resultado_query = mysqli_query($mysqli, $resultado_pesquisa);
                                    
                                   // if ($ind->num_rows == 1) {

                                    if ($resultado_query->num_rows == 1) {
                                        while ($proc1 = mysqli_fetch_assoc($resultado_query)) { ?>
                                <tr>
                                    <td align="center">
                                        <a class="btn btn-default"><em class="fa fa-pencil"></em></a>
                                        <a class="btn btn-danger"><em class="fa fa-trash"></em></a>
                                    </td>
                                    <td><?php echo $proc1['id'] . "<br/>"; ?></td>
                                    <td><?php echo $proc1['nome'] . "<br/>"; ?></td>
                                    <td><?php echo $proc1['email'] . "<br/>"; ?></td>
                                    <td><?php echo $proc1['senha'] . "<br/>"; ?></td>
                                </tr>
                                <?php
                                        }
                                    } else {
                                        echo "<h4>ID Não encontrado </h4>";
                                    }
                                //}else {
                                 //   echo ("Faça uma pesquisa !");
                                //}

                                ?>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col col-xs-4"></div>
                            <div class="col col-xs-8">
                                <ul class="pagination hidden-xs pull-right"></ul>
                                <p>
                                    <b>Deseja sair ?</b> <a href="login.php?v=back"><button> Sair</button> </a>

                                </p>
                                <ul class="pagination visible-xs pull-right">
                                    <li><a href="#">«</a></li>
                                    <li><a href="#">»</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php 
       $enviar=($_POST[("enviar")]);
    ?>
    <form action="" method="POST">
        
        <button type=submit name=enviar> LISTAR TODOS </button>
        <?php
        $sql = mysqli_query($mysqli, "SELECT * FROM acesso_novo");
        if ($sql->num_rows ==1) {
            
            while ($proc = mysqli_fetch_assoc($sql)) {
                echo "<br/>";
                echo "-----------------------------------------<br />";
                echo "ID: " . $proc['id'] . "<br/>";
                echo "Email: " . $proc['email'] . "<br/>";
                echo "Senha: " . $proc['senha'];
            }
        }

        ?>

    </form>


</body>

</html>